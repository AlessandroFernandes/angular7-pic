import { Directive, ElementRef, HostListener, Renderer, Input } from '@angular/core';

@Directive({
  selector: '[sfDarkenOnHover]'
})
export class DarkenOnHoverDirective{

  @Input() brightness = '70%';

  constructor(private el: ElementRef, private render: Renderer){}

  @HostListener('mouseover')
  darkeOn(){
    this.render.setElementStyle(this.el.nativeElement, 'filter', `brightness(${this.brightness})`)
  }

  @HostListener('mouseleave')
  darkeOff(){
    this.render.setElementStyle(this.el.nativeElement, 'filter', 'brightness(100%)')
  }


}

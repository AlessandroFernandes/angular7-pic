import { Component, OnInit, OnDestroy} from '@angular/core';
import { ActivatedRoute } from '@angular/router';


import { Photo } from '../photo/photo';
import { PhotoService } from '../photo/photo.service';


@Component({
  selector: 'app-photo-list',
  templateUrl: './photo-list.component.html',
  styleUrls: ['./photo-list.component.css']
})

export class PhotoListComponent implements OnInit {

  photos: Photo[] = [];
  filter: String = '';
  hasMore: boolean = true;
  currentPage: number = 1;
  userName: string = '';

  constructor(private ActivatedRoute: ActivatedRoute, private PhotoService: PhotoService){}


  ngOnInit(): void {
    this.userName = this.ActivatedRoute.snapshot.params.userName;
    this.photos = this.ActivatedRoute.snapshot.data['photos'];
  }

  load(){
    this.PhotoService.listPhotoPaginated(this.userName, ++this.currentPage)
    .subscribe(photos => {
      this.filter = '';
      this.photos = this.photos.concat(photos);
      if(!photos.length) this.hasMore = false;
    })
  }

}

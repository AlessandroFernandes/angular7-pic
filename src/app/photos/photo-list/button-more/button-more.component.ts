import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'sf-button-more',
  templateUrl: './button-more.component.html',
  styleUrls: ['./button-more.component.css']
})
export class ButtonMoreComponent implements OnInit {


  constructor() { }

  @Input() hasMore : boolean = false;

  ngOnInit() {
  }

}


import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';

import { Photo } from '../photo/photo';
import { PhotoService } from '../photo/photo.service';

@Injectable({ providedIn: 'root'})
export class PhotoRouteResolver implements Resolve<Observable<Photo[]>>{

  constructor(private Service: PhotoService){}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<Photo[]>{

    const userName = route.params.userName;
    return this.Service.listPhotoPaginated(userName, 1);

  }

}

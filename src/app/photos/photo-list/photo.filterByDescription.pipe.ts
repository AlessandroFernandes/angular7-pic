import { PipeTransform, Pipe } from '@angular/core';
import { Photo } from '../photo/photo';

@Pipe({name: 'filterByDescription'})
export class FilterByDescription implements PipeTransform {

  transform(photo: Photo[], description: string) {

    const data = description.trim().toLowerCase();

    if(data){
      return  photo.filter(photos => photos.description.trim().toLowerCase().includes(data))
    }else{
      return photo
    }
  }

}

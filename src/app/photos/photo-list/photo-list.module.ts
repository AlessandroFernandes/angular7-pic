import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PhotosComponent } from './photos/photos.component';
import { PhotoListComponent } from './photo-list.component';
import { ButtonMoreComponent } from './button-more/button-more.component';
import { FilterByDescription } from './photo.filterByDescription.pipe';
import { PhotoModule } from '../photo/photo.module';
import { CardModule } from 'src/app/shared/components/card/card.module';
import { SearchComponent } from './search/search.component';
import { DarkenOnHoverModule } from 'src/app/shared/directives/darken-on-hover/darken-on-hover.module';

@NgModule({
  declarations: [PhotosComponent,
                PhotoListComponent,
                ButtonMoreComponent,
                FilterByDescription,
                SearchComponent],

  imports: [PhotoModule, CardModule, CommonModule,  DarkenOnHoverModule],
})
export class PhotoListModule{}

import { Component, OnInit, OnDestroy, Output, EventEmitter, Input } from '@angular/core';
import { Subject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: "sf-search",
  templateUrl: "./search.component.html"
})
export class SearchComponent implements OnInit, OnDestroy{

  debounce: Subject<string> = new Subject<string>();
  @Output() onDados: EventEmitter<string> = new EventEmitter<string>();
  @Input() value: string = '';

  constructor(){}

  ngOnInit(): void {
    this.debounce
    .pipe(debounceTime(300))
    .subscribe(data => this.onDados.emit(data))
  }

  ngOnDestroy(): void {
    this.debounce.unsubscribe();
  }


}
